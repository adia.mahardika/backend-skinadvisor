const fetch = require("node-fetch");

module.exports = {
  getAllProduct: async () => {
    return new Promise((resolve, reject) => {
      fetch("https://www.avoskinbeauty.com/wp-json/avoskin/v1/get_product_list?total=100")
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          reject(console.log(error));
        });
    });
  },
};
