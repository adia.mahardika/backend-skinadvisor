const multer = require("multer");
const crypto = require("crypto");
const sharp = require("sharp");

const filename = (request, file, callback) => {
  let customFileName = Date.now() + crypto.randomBytes(6).toString("hex"),
    fileExtension = file.originalname.split(".")[
      file.originalname.split(".").length - 1
    ];
  callback(null, customFileName + "." + fileExtension);
};
const imageFilter = (request, file, callback) => {
  const imageFilter = file.mimetype.toLowerCase();
  if (
    imageFilter === "image/svg+xml" ||
    imageFilter === "image/png" ||
    imageFilter === "image/jpeg" ||
    imageFilter === "image/jpg"
  ) {
    return callback(null, true);
  } else {
    return callback(
      null,
      false,
      new Error(
        "Just image with extension .svg, .png, .jpg, and .jpeg can be upload!"
      )
    );
  }
};
const imageLimits = {
  fileSize: 1024 * 1024 * 10,
};

module.exports = {
  compress: async (path) => {
    await sharp("./" + path)
      .toBuffer()
      .then(async (data) => {
        await sharp(data)
          .jpeg({
            quality: 80,
          })
          .toFile("./" + path);
      });
  },
  uploadHome: multer({
    fileFilter: imageFilter,
    limits: imageLimits,
    storage: multer.diskStorage({
      destination: (request, file, callback) => {
        callback(null, "./upload/home");
      },
      filename,
    }),
  }).single("image"),
  uploadBasicInfo: multer({
    fileFilter: imageFilter,
    limits: imageLimits,
    storage: multer.diskStorage({
      destination: (request, file, callback) => {
        callback(null, "./upload/basic_info");
      },
      filename,
    }),
  }).single("image"),
  uploadInfoKondisiKulit: multer({
    fileFilter: imageFilter,
    limits: imageLimits,
    storage: multer.diskStorage({
      destination: (request, file, callback) => {
        callback(null, "./upload/info_kondisi_kulit");
      },
      filename,
    }),
  }).single("image"),
  uploadHomeCard: multer({
    fileFilter: imageFilter,
    limits: imageLimits,
    storage: multer.diskStorage({
      destination: (request, file, callback) => {
        callback(null, "./upload/home_card");
      },
      filename,
    }),
  }).single("image"),
  uploadMasalahKulit: multer({
    fileFilter: imageFilter,
    limits: imageLimits,
    storage: multer.diskStorage({
      destination: (request, file, callback) => {
        callback(null, "./upload/masalah_kulit");
      },
      filename,
    }),
  }).single("image"),
  uploadKondisiKulit: multer({
    fileFilter: imageFilter,
    limits: imageLimits,
    storage: multer.diskStorage({
      destination: (request, file, callback) => {
        callback(null, "./upload/kondisi_kulit");
      },
      filename,
    }),
  }).single("image"),
};
