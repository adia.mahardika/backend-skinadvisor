const connection = require("../configs/mysql");

const readQuery = `SELECT alergi_kulit.*, alergi_product.id_product FROM alergi_kulit LEFT JOIN alergi_product ON alergi_kulit.id = alergi_product.id_alergi_kulit ORDER BY name ASC`;

module.exports = {
  getAlergiKulit: (search_name) => {
    return new Promise((resolve, reject) => {
      connection.query(`SELECT alergi_kulit.*, alergi_product.id_product FROM alergi_kulit LEFT JOIN alergi_product ON alergi_kulit.id = alergi_product.id_alergi_kulit WHERE alergi_kulit.name LIKE '%${search_name}%' ORDER BY name ASC`, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  insertAlergiKulit: (data) => {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO alergi_kulit SET ?", data);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  updateAlergiKulit: (data, alergi_kulit_id) => {
    return new Promise((resolve, reject) => {
      connection.query("UPDATE alergi_kulit SET ? WHERE id = ?", [data, alergi_kulit_id]);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  deleteAlergiKulit: (alergi_kulit_id) => {
    return new Promise((resolve, reject) => {
      connection.query("DELETE FROM alergi_kulit WHERE id = ?", alergi_kulit_id);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
};
