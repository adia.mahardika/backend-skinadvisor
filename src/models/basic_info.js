const connection = require("../configs/mysql");

const readQuery = `SELECT * FROM basic_info ORDER BY title ASC`;

module.exports = {
  getBasicInfo: () => {
    return new Promise((resolve, reject) => {
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  insertBasicInfo: (data) => {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO basic_info SET ?", data);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  checkId: (basic_info_id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        `SELECT * FROM basic_info WHERE id = ?`,
        basic_info_id,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  updateBasicInfo: (data, basic_info_id) => {
    return new Promise((resolve, reject) => {
      connection.query("UPDATE basic_info SET ? WHERE id = ?", [
        data,
        basic_info_id,
      ]);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  deleteBasicInfo: (basic_info_id) => {
    return new Promise((resolve, reject) => {
      connection.query("DELETE FROM basic_info WHERE id = ?", basic_info_id);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
};
