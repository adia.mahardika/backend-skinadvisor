const connection = require("../configs/mysql");

const readQuery = `SELECT alergi_product.id, alergi_product.id_alergi_kulit, alergi_kulit.name AS alergi_kulit_name, alergi_product.id_product FROM alergi_product LEFT JOIN alergi_kulit ON alergi_product.id_alergi_kulit = alergi_kulit.id ORDER BY alergi_kulit.name ASC `;

module.exports = {
  getAlergiProduct: (search_name) => {
    return new Promise((resolve, reject) => {
      connection.query(
        `SELECT alergi_product.id, alergi_product.id_alergi_kulit, alergi_kulit.name AS alergi_kulit_name, alergi_product.id_product FROM alergi_product LEFT JOIN alergi_kulit ON alergi_product.id_alergi_kulit = alergi_kulit.id WHERE alergi_kulit.name LIKE '%${search_name}%' ORDER BY alergi_kulit.name ASC`,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  insertAlergiProduct: (data) => {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO alergi_product SET ?", data);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  updateAlergiProduct: (data, alergi_product_id) => {
    return new Promise((resolve, reject) => {
      connection.query("UPDATE alergi_product SET ? WHERE id = ?", [
        data,
        alergi_product_id,
      ]);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  deleteAlergiProduct: (alergi_product_id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM alergi_product WHERE id = ?",
        alergi_product_id
      );
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
};
