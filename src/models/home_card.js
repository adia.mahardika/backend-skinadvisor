const connection = require("../configs/mysql");

const readQuery = `SELECT * FROM home_card ORDER BY date_updated DESC`;

module.exports = {
  getHomeCard: () => {
    return new Promise((resolve, reject) => {
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  insertHomeCard: (data) => {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO home_card SET ?", data);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  checkId: (home_card_id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        `SELECT * FROM home_card WHERE id = ?`,
        home_card_id,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  updateHomeCard: (data, home_card_id) => {
    return new Promise((resolve, reject) => {
      connection.query("UPDATE home_card SET ? WHERE id = ?", [data, home_card_id]);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  deleteHomeCard: (home_card_id) => {
    return new Promise((resolve, reject) => {
      connection.query("DELETE FROM home_card WHERE id = ?", home_card_id);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
};
