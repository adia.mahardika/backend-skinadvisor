const connection = require("../configs/mysql");
const readQuery = `SELECT * FROM admin ORDER BY name ASC`;
module.exports = {
  register: (data) => {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO admin SET ?", data);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  checkEmail: (email) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "SELECT * FROM admin WHERE email = ?",
        email,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  getAllAdmin: () => {
    return new Promise((resolve, reject) => {
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  updateAdmin: (data, id) => {
    return new Promise((resolve, reject) => {
      connection.query("UPDATE admin SET ? WHERE id = ?", [data, id]);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  deleteAdmin: (id) => {
    return new Promise((resolve, reject) => {
      connection.query("DELETE FROM admin WHERE id = ?", id);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
};
