const connection = require("../configs/mysql");
const { parseDate } = require("../helpers/index");

const readQuery = `SELECT user.id, user.name, user.birth_date, user.birth_date, user.gender, alergi_kulit.name AS alergi_kulit, alergi_kulit_user.description AS alergi_kulit_user_description, masalah_kulit.name AS masalah_kulit, kondisi_kulit.name AS kondisi_kulit, user.date_updated, user.date_created FROM user LEFT JOIN alergi_kulit_user ON user.alergi_kulit_user = alergi_kulit_user.id LEFT JOIN alergi_kulit ON alergi_kulit_user.id_alergi_kulit = alergi_kulit.id LEFT JOIN masalah_kulit_user ON user.masalah_kulit_user = masalah_kulit_user.id LEFT JOIN masalah_kulit ON masalah_kulit_user.id_masalah_kulit = masalah_kulit.id LEFT JOIN kondisi_kulit_user ON user.kondisi_kulit_user = kondisi_kulit_user.id JOIN kondisi_kulit ON kondisi_kulit_user.id_kondisi_kulit = kondisi_kulit.id ORDER BY name ASC LIMIT 0, 10`;

const finalResult = async (result) => {
  let newResult = [];
  await result.map((value) => {
    const findIndex = newResult.findIndex(
      (newResultValue) => newResultValue.id === value.id
    );
    if (findIndex < 0) {
      newResult.push({
        ...value,
        date_updated: parseDate(value.date_updated),
        date_created: parseDate(value.date_created),
        kondisi_kulit: [value.kondisi_kulit],
        masalah_kulit: [value.masalah_kulit],
        alergi_kulit: [value.alergi_kulit],
      });
    } else {
      const index_kondisi_kulit = newResult[findIndex].kondisi_kulit.findIndex(
        (kondisi_kulit_value) => kondisi_kulit_value === value.kondisi_kulit
      );
      const index_masalah_kulit = newResult[findIndex].masalah_kulit.findIndex(
        (masalah_kulit_value) => masalah_kulit_value === value.masalah_kulit
      );
      const index_alergi_kulit = newResult[findIndex].alergi_kulit.findIndex(
        (alergi_kulit_value) => alergi_kulit_value === value.alergi_kulit
      );
      if (index_kondisi_kulit < 0) {
        newResult[findIndex].kondisi_kulit.push(value.kondisi_kulit);
      }
      if (index_masalah_kulit < 0) {
        newResult[findIndex].masalah_kulit.push(value.masalah_kulit);
      }
      if (index_alergi_kulit < 0) {
        newResult[findIndex].alergi_kulit.push(value.alergi_kulit);
      }
    }
  });
  return newResult;
};
module.exports = {
  getUser: (
    startDate,
    endDate,
    searchName,
    sortBy,
    orderBy,
    startIndex,
    limit
  ) => {
    if (startDate !== "" && endDate !== "") {
      return new Promise((resolve, reject) => {
        connection.query(
          `SELECT user.id, user.name, user.birth_date, user.birth_date, user.gender, alergi_kulit.name AS alergi_kulit, alergi_kulit_user.description AS alergi_kulit_user_description, masalah_kulit.name AS masalah_kulit, kondisi_kulit.name AS kondisi_kulit, user.date_updated, user.date_created FROM user LEFT JOIN alergi_kulit_user ON user.alergi_kulit_user = alergi_kulit_user.id LEFT JOIN alergi_kulit ON alergi_kulit_user.id_alergi_kulit = alergi_kulit.id LEFT JOIN masalah_kulit_user ON user.masalah_kulit_user = masalah_kulit_user.id LEFT JOIN masalah_kulit ON masalah_kulit_user.id_masalah_kulit = masalah_kulit.id LEFT JOIN kondisi_kulit_user ON user.kondisi_kulit_user = kondisi_kulit_user.id JOIN kondisi_kulit ON kondisi_kulit_user.id_kondisi_kulit = kondisi_kulit.id WHERE DATE(user.date_created) BETWEEN '${startDate}' AND '${endDate}' AND user.name LIKE '%${searchName}%' ORDER BY ${sortBy} ${orderBy} LIMIT ${startIndex}, ${limit}`,
          (error, result) => {
            if (error) reject(new Error(error));
            let final_result = finalResult(result);
            resolve(final_result);
          }
        );
      });
    } else {
      return new Promise((resolve, reject) => {
        connection.query(
          `SELECT user.id, user.name, user.birth_date, user.birth_date, user.gender, alergi_kulit.name AS alergi_kulit, alergi_kulit_user.description AS alergi_kulit_user_description, masalah_kulit.name AS masalah_kulit, kondisi_kulit.name AS kondisi_kulit, user.date_updated, user.date_created FROM user LEFT JOIN alergi_kulit_user ON user.alergi_kulit_user = alergi_kulit_user.id LEFT JOIN alergi_kulit ON alergi_kulit_user.id_alergi_kulit = alergi_kulit.id LEFT JOIN masalah_kulit_user ON user.masalah_kulit_user = masalah_kulit_user.id LEFT JOIN masalah_kulit ON masalah_kulit_user.id_masalah_kulit = masalah_kulit.id LEFT JOIN kondisi_kulit_user ON user.kondisi_kulit_user = kondisi_kulit_user.id JOIN kondisi_kulit ON kondisi_kulit_user.id_kondisi_kulit = kondisi_kulit.id WHERE user.name LIKE '%${searchName}%' ORDER BY ${sortBy} ${orderBy} LIMIT ${startIndex}, ${limit}`,
          async (error, result) => {
            if (error) reject(new Error(error));
            let final_result = await finalResult(result);
            resolve(final_result);
          }
        );
      });
    }
  },
  insertUser: (data) => {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO user SET ?", data);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        let final_result = finalResult(result);
        resolve(final_result);
      });
    });
  },
  insertMasalahKulitUser: (data) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "INSERT INTO masalah_kulit_user SET ?",
        data,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  insertKondisiKulitUser: (data) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "INSERT INTO kondisi_kulit_user SET ?",
        data,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  insertAlergiKulitUser: (data) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "INSERT INTO alergi_kulit_user SET ?",
        data,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  updateUser: (data, user_id) => {
    return new Promise((resolve, reject) => {
      connection.query("UPDATE user SET ? WHERE id = ?", [data, user_id]);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        let final_result = finalResult(result);
        resolve(final_result);
      });
    });
  },
  deleteUser: (user_id) => {
    return new Promise((resolve, reject) => {
      connection.query("DELETE FROM user WHERE id = ?", user_id);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        let final_result = finalResult(result);
        resolve(final_result);
      });
    });
  },
  deleteAlergiKulitUser: (user_id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM alergi_kulit_user WHERE id_user = ?",
        user_id,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  deleteKondisiKulitUser: (user_id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM kondisi_kulit_user WHERE id_user = ?",
        user_id,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  deleteMasalahKulitUser: (user_id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM masalah_kulit_user WHERE id_user = ?",
        user_id,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  countData: (searchName, sortBy, orderBy, startDate, endDate) => {
    if (startDate !== "" && endDate !== "") {
      return new Promise((resolve, reject) => {
        connection.query(
          `SELECT count(*) as total_data FROM user WHERE DATE(user.date_created) BETWEEN '${startDate}' AND '${endDate}' AND user.name LIKE '%${searchName}%' ORDER BY ${sortBy} ${orderBy}`,
          (error, result) => {
            if (error) reject(new Error(error));
            resolve(result[0].total_data);
          }
        );
      });
    } else {
      return new Promise((resolve, reject) => {
        connection.query(
          `SELECT count(*) as total_data FROM user WHERE user.name LIKE '%${searchName}%' ORDER BY ${sortBy} ${orderBy}`,
          (error, result) => {
            if (error) reject(new Error(error));
            resolve(result[0].total_data);
          }
        );
      });
    }
  },
};
