const connection = require("../configs/mysql");

const readQuery = `SELECT * FROM home ORDER BY title ASC`;

module.exports = {
  getHome: () => {
    return new Promise((resolve, reject) => {
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  insertHome: (data) => {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO home SET ?", data);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  checkId: (home_id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        `SELECT * FROM home WHERE id = ?`,
        home_id,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  updateHome: (data, home_id) => {
    return new Promise((resolve, reject) => {
      connection.query("UPDATE home SET ? WHERE id = ?", [data, home_id]);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  deleteHome: (home_id) => {
    return new Promise((resolve, reject) => {
      connection.query("DELETE FROM home WHERE id = ?", home_id);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
};
