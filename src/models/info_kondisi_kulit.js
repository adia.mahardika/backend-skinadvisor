const connection = require("../configs/mysql");

const readQuery = `SELECT info_kondisi_kulit.*, info_kondisi_kulit_image.image, info_kondisi_kulit_image.id AS image_id FROM info_kondisi_kulit LEFT JOIN info_kondisi_kulit_image ON info_kondisi_kulit.id = info_kondisi_kulit_image.id_info_kondisi_kulit ORDER BY title ASC`;

let finalResult = (result) => {
  let newResult = [];
  result.map((value) => {
    const findIndex = newResult.findIndex(
      (newResultValue) => newResultValue.id === value.id
    );
    if (findIndex < 0) {
      newResult.push({
        ...value,
        image: [{ image_id: value.image_id, image: value.image }],
      });
    } else {
      delete newResult[findIndex].image_id;
      newResult[findIndex].image.push({
        image_id: value.image_id,
        image: value.image,
      });
    }
  });
  return newResult;
};
module.exports = {
  getInfoKondisiKulit: () => {
    return new Promise((resolve, reject) => {
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        let final_result = finalResult(result);
        resolve(final_result);
      });
    });
  },
  insertInfoKondisiKulit: (data) => {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO info_kondisi_kulit SET ?", data);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        let final_result = finalResult(result);
        resolve(final_result);
      });
    });
  },
  checkId: (info_kondisi_kulit_id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        `SELECT * FROM info_kondisi_kulit WHERE id = ?`,
        info_kondisi_kulit_id,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  updateInfoKondisiKulit: (data, info_kondisi_kulit_id) => {
    return new Promise((resolve, reject) => {
      connection.query("UPDATE info_kondisi_kulit SET ? WHERE id = ?", [
        data,
        info_kondisi_kulit_id,
      ]);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        let final_result = finalResult(result);
        resolve(final_result);
      });
    });
  },
  deleteInfoKondisiKulit: (info_kondisi_kulit_id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM info_kondisi_kulit WHERE id = ?",
        info_kondisi_kulit_id
      );
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        let final_result = finalResult(result);
        resolve(final_result);
      });
    });
  },
};
