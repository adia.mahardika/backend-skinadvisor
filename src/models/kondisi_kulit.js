const connection = require("../configs/mysql");

const readQuery = `SELECT kondisi_kulit.*, kondisi_kulit_product.id_product FROM kondisi_kulit LEFT JOIN kondisi_kulit_product ON  kondisi_kulit.id = kondisi_kulit_product.id_kondisi_kulit ORDER BY name ASC`;

module.exports = {
  getKondisiKulit: (search_name) => {
    return new Promise((resolve, reject) => {
      connection.query(
        `SELECT kondisi_kulit.*, kondisi_kulit_product.id_product FROM kondisi_kulit LEFT JOIN kondisi_kulit_product ON  kondisi_kulit.id = kondisi_kulit_product.id_kondisi_kulit WHERE kondisi_kulit.name LIKE '%${search_name}%' ORDER BY name ASC`,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  insertKondisiKulit: (data) => {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO kondisi_kulit SET ?", data);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  insertKondisiKulitProduct: (data) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "INSERT INTO kondisi_kulit_product SET ?",
        data,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  updateKondisiKulit: (data, kondisi_kulit_id) => {
    return new Promise((resolve, reject) => {
      connection.query("UPDATE kondisi_kulit SET ? WHERE id = ?", [
        data,
        kondisi_kulit_id,
      ]);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  deleteKondisiKulit: (kondisi_kulit_id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM kondisi_kulit WHERE id = ?",
        kondisi_kulit_id
      );
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  deleteKondisiKulitProduct: (id_kondisi_kulit) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM kondisi_kulit_product WHERE id_kondisi_kulit = ?",
        id_kondisi_kulit,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  checkId: (id_kondisi_kulit) => {
    return new Promise((resolve, reject) => {
      connection.query(
        `SELECT * FROM kondisi_kulit WHERE id = ?`,
        id_kondisi_kulit,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
};
