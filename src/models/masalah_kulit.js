const connection = require("../configs/mysql");

const readQuery = `SELECT masalah_kulit.*, masalah_kulit_product.id_product FROM masalah_kulit LEFT JOIN masalah_kulit_product ON  masalah_kulit.id = masalah_kulit_product.id_masalah_kulit ORDER BY name ASC`;

module.exports = {
  getMasalahKulit: (search_name) => {
    return new Promise((resolve, reject) => {
      connection.query(
        `SELECT masalah_kulit.*, masalah_kulit_product.id_product FROM masalah_kulit LEFT JOIN masalah_kulit_product ON  masalah_kulit.id = masalah_kulit_product.id_masalah_kulit WHERE masalah_kulit.name LIKE '%${search_name}%' ORDER BY name ASC`,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  insertMasalahKulit: (data) => {
    return new Promise((resolve, reject) => {
      connection.query("INSERT INTO masalah_kulit SET ?", data);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  insertMasalahKulitProduct: (data) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "INSERT INTO masalah_kulit_product SET ?",
        data,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  updateMasalahKulit: (data, masalah_kulit_id) => {
    return new Promise((resolve, reject) => {
      connection.query("UPDATE masalah_kulit SET ? WHERE id = ?", [
        data,
        masalah_kulit_id,
      ]);
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  deleteMasalahKulit: (masalah_kulit_id) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM masalah_kulit WHERE id = ?",
        masalah_kulit_id
      );
      connection.query(readQuery, (error, result) => {
        if (error) reject(new Error(error));
        resolve(result);
      });
    });
  },
  deleteMasalahKulitProduct: (id_masalah_kulit) => {
    return new Promise((resolve, reject) => {
      connection.query(
        "DELETE FROM masalah_kulit_product WHERE id_masalah_kulit = ?",
        id_masalah_kulit,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
  checkId: (id_masalah_kulit) => {
    return new Promise((resolve, reject) => {
      connection.query(
        `SELECT * FROM masalah_kulit WHERE id = ?`,
        id_masalah_kulit,
        (error, result) => {
          if (error) reject(new Error(error));
          resolve(result);
        }
      );
    });
  },
};
