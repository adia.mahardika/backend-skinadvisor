const express = require("express");
const Route = express.Router();

const {
  getHomeCard,
  insertHomeCard,
  updateHomeCard,
  deleteHomeCard,
} = require("../controllers/home_card");
const { uploadHomeCard } = require("../helpers/upload");

Route.post("/", uploadHomeCard, insertHomeCard)
  .get("/", getHomeCard)
  .patch("/:home_card_id", uploadHomeCard, updateHomeCard)
  .delete("/:home_card_id", deleteHomeCard);
module.exports = Route;
