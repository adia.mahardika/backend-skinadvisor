const express = require("express");
const Route = express.Router();

const {
  getHome,
  insertHome,
  updateHome,
  deleteHome,
} = require("../controllers/home");
const { uploadHome } = require("../helpers/upload");

Route.post("/", uploadHome, insertHome)
  .get("/", getHome)
  .patch("/:home_id", uploadHome, updateHome)
  .delete("/:home_id", deleteHome);
module.exports = Route;
