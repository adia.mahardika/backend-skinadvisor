const express = require("express");
const Route = express.Router();

const homeRouter = require("./home");
const homeCardRouter = require("./home_card");
const userRouter = require("./user");
const kondisiKulitRouter = require("./kondisi_kulit");
const alergiKulitRouter = require("./alergi_kulit");
const masalahKulitRouter = require("./masalah_kulit");
const alergiProductRouter = require("./alergi_product");
const basicInfoRouter = require("./basic_info");
const infoKondisiKulit = require("./info_kondisi_kulit");
const infoKondisiKulitImage = require("./info_kondisi_kulit_image");
const adminRouter = require("./admin")
Route.use("/upload", express.static("./upload"))
  .use("/home", homeRouter)
  .use("/home-card", homeCardRouter)
  .use("/user", userRouter)
  .use("/kondisi-kulit", kondisiKulitRouter)
  .use("/alergi-kulit", alergiKulitRouter)
  .use("/masalah-kulit", masalahKulitRouter)
  .use("/alergi-product", alergiProductRouter)
  .use("/basic-info", basicInfoRouter)
  .use("/info-kondisi-kulit", infoKondisiKulit)
  .use("/info-kondisi-kulit-image", infoKondisiKulitImage)
  .use("/admin", adminRouter)
module.exports = Route;
