const express = require("express");
const Route = express.Router();

const {
  getAlergiKulit,
  insertAlergiKulit,
  updateAlergiKulit,
  deleteAlergiKulit,
} = require("../controllers/alergi_kulit");

Route.post("/", insertAlergiKulit)
  .get("/", getAlergiKulit)
  .patch("/:alergi_kulit_id", updateAlergiKulit)
  .delete("/:alergi_kulit_id", deleteAlergiKulit);
module.exports = Route;
