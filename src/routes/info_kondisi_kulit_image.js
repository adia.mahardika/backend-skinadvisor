const express = require("express");
const Route = express.Router();

const {
  getInfoKondisiKulitImage,
  insertInfoKondisiKulitImage,
  updateInfoKondisiKulitImage,
  deleteInfoKondisiKulitImage,
} = require("../controllers/info_kondisi_kulit_image");
const { uploadInfoKondisiKulit } = require("../helpers/upload");

Route.post("/", uploadInfoKondisiKulit, insertInfoKondisiKulitImage)
  .get("/", getInfoKondisiKulitImage)
  .patch(
    "/:info_kondisi_kulit_image_id",
    uploadInfoKondisiKulit,
    updateInfoKondisiKulitImage
  )
  .delete("/:info_kondisi_kulit_image_id", deleteInfoKondisiKulitImage);
module.exports = Route;
