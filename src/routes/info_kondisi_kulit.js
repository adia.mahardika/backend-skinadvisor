const express = require("express");
const Route = express.Router();

const {
  getInfoKondisiKulit,
  insertInfoKondisiKulit,
  updateInfoKondisiKulit,
  deleteInfoKondisiKulit,
} = require("../controllers/info_kondisi_kulit");

Route.post("/", insertInfoKondisiKulit)
  .get("/", getInfoKondisiKulit)
  .patch("/:info_kondisi_kulit_id", updateInfoKondisiKulit)
  .delete("/:info_kondisi_kulit_id", deleteInfoKondisiKulit);
module.exports = Route;
