const express = require("express");
const Route = express.Router();

const {
  getBasicInfo,
  insertBasicInfo,
  updateBasicInfo,
  deleteBasicInfo,
} = require("../controllers/basic_info");
const { uploadBasicInfo } = require("../helpers/upload");

Route.post("/", uploadBasicInfo, insertBasicInfo)
  .get("/", getBasicInfo)
  .patch("/:basic_info_id", uploadBasicInfo, updateBasicInfo)
  .delete("/:basic_info_id", deleteBasicInfo);
module.exports = Route;
