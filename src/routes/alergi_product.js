const express = require("express");
const Route = express.Router();

const {
  getAlergiProduct,
  insertAlergiProduct,
  updateAlergiProduct,
  deleteAlergiProduct,
} = require("../controllers/alergi_product");

Route.post("/", insertAlergiProduct)
  .get("/", getAlergiProduct)
  .patch("/:alergi_product_id", updateAlergiProduct)
  .delete("/:alergi_product_id", deleteAlergiProduct);
module.exports = Route;
