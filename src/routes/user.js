const express = require("express");
const Route = express.Router();

const {
  getUser,
  insertUser,
  updateUser,
  deleteUser,
} = require("../controllers/user");

Route.post("/", insertUser)
  .get("/", getUser)
  .patch("/:user_id", updateUser)
  .delete("/:user_id", deleteUser);
module.exports = Route;
