const express = require("express");
const Route = express.Router();

const {
  register,
  login,
  token,
  getAllAdmin,
  updateAdmin,
  deleteAdmin,
} = require("../controllers/admin");
Route.post("/register", register)
  .post("/login", login)
  .post("/token", token)
  .get("/", getAllAdmin)
  .get("/:id", getAllAdmin)
  .patch("/:id", updateAdmin)
  .delete("/:id", deleteAdmin);

module.exports = Route;
