const express = require("express");
const Route = express.Router();

const {
  getMasalahKulit,
  insertMasalahKulit,
  updateMasalahKulit,
  deleteMasalahKulit,
} = require("../controllers/masalah_kulit");

const { uploadMasalahKulit } = require("../helpers/upload");

Route.post("/", uploadMasalahKulit, insertMasalahKulit)
  .get("/", getMasalahKulit)
  .patch("/:id_masalah_kulit", uploadMasalahKulit, updateMasalahKulit)
  .delete("/:id_masalah_kulit", deleteMasalahKulit);
module.exports = Route;
