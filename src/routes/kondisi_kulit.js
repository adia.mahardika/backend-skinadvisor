const express = require("express");
const Route = express.Router();

const {
  getKondisiKulit,
  insertKondisiKulit,
  updateKondisiKulit,
  deleteKondisiKulit,
} = require("../controllers/kondisi_kulit");

const { uploadKondisiKulit } = require("../helpers/upload");

Route.post("/", uploadKondisiKulit, insertKondisiKulit)
  .get("/", getKondisiKulit)
  .patch("/:id_kondisi_kulit", uploadKondisiKulit, updateKondisiKulit)
  .delete("/:id_kondisi_kulit", deleteKondisiKulit);
module.exports = Route;
