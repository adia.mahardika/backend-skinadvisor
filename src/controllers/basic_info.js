const basicInfoModel = require("../models/basic_info");
const miscHelper = require("../helpers");
const filesystem = require("fs").promises;
const { ip, image_path } = require("../configs");
const uniqid = require("uniqid");

const deleteFile = async (id) => {
  const checkId = await basicInfoModel.checkId(id);
  const data = checkId[0];
  if (data !== undefined) {
    const path = data.image.replace(`http://${ip}`, `${image_path}`);
    await filesystem.unlink(path);
  }
};
module.exports = {
  getBasicInfo: async (request, response) => {
    try {
      const result = await basicInfoModel.getBasicInfo();
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error)
      miscHelper.customErrorResponse(response, 404, "Cannot get basic info!");
    }
  },
  insertBasicInfo: async (request, response) => {
    try {
      const title = request.body.title;
      const id =
      title.toLowerCase().split(" ").join("-") + "-" + uniqid.time();
      const data = {
        id,
        title,
        subtitle: request.body.subtitle,
        image: `http://${ip}/upload/basic_info/${request.file.filename}`,
        date_created: new Date(),
        date_updated: new Date(),
      };
      const result = await basicInfoModel.insertBasicInfo(data);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot insert basic info!");
    }
  },
  updateBasicInfo: async (request, response) => {
    try {
      const basic_info_id = request.params.basic_info_id;
      if (!request.file || Object.keys(request.file).length === 0) {
        const data = {
          title: request.body.title,
          subtitle: request.body.subtitle,
          date_updated: new Date(),
        };
        const result = await basicInfoModel.updateBasicInfo(data, basic_info_id);
        miscHelper.customResponse(response, 200, result);
      } else {
        await deleteFile(basic_info_id);
        const data = {
          title: request.body.title,
          subtitle: request.body.subtitle,
          image: `http://${ip}/upload/basic_info/${request.file.filename}`,
          date_updated: new Date(),
        };
        const result = await basicInfoModel.updateBasicInfo(data, basic_info_id);
        miscHelper.customResponse(response, 200, result);
      }
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot update basic info!");
    }
  },
  deleteBasicInfo: async (request, response) => {
    try {
      const basic_info_id = request.params.basic_info_id;
      await deleteFile(basic_info_id);
      const result = await basicInfoModel.deleteBasicInfo(basic_info_id);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot delete basic info!");
    }
  },
};
