const alergiKulitModel = require("../models/alergi_kulit");
const miscHelper = require("../helpers");
const uniqid = require("uniqid");

const { getAllProduct } = require("../helpers/product");

const finalResult = async (result) => {
  const allProduct = await getAllProduct();
  const finalResult = [];
  await result.map((resultValue) => {
    let product = { title: "Product Not Found" };
    allProduct.data.filter((productValue) => {
      if (resultValue.id_product === productValue.id) {
        let newPrice = productValue.price.replace(/[^0-9 ]/g, "");
        return (product = {
          ...productValue,
          price: parseInt(newPrice),
          cart_quantity: 0,
        });
      }
    });
    let newResult = { ...resultValue, product };
    const findIndex = finalResult.findIndex(
      (findIndexValue) => findIndexValue.id === resultValue.id
    );
    if (findIndex < 0) {
      return finalResult.push({
        ...newResult,
        id_product: [product.id],
        product: [product],
      });
    } else {
      return [
        finalResult[findIndex].product.push(product),
        finalResult[findIndex].id_product.push(product.id),
      ];
    }
  });
  return finalResult;
};

module.exports = {
  getAlergiKulit: async (request, response) => {
    try {
      const search_name = request.query.name || "";
      const result = await alergiKulitModel.getAlergiKulit(search_name);
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot get alergi kulit!");
    }
  },
  insertAlergiKulit: async (request, response) => {
    try {
      const id = uniqid.process();
      const data = {
        id,
        name: request.body.name,
        description: request.body.description || "",
        date_created: new Date(),
        date_updated: new Date(),
      };
      const result = await alergiKulitModel.insertAlergiKulit(data);
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot insert alergi kulit!"
      );
    }
  },
  updateAlergiKulit: async (request, response) => {
    try {
      const alergi_kulit_id = request.params.alergi_kulit_id;
      const data = {
        name: request.body.name,
        description: request.body.description,
        date_updated: new Date(),
      };
      const result = await alergiKulitModel.updateAlergiKulit(
        data,
        alergi_kulit_id
      );
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot update alergi kulit!"
      );
    }
  },
  deleteAlergiKulit: async (request, response) => {
    try {
      const alergi_kulit_id = request.params.alergi_kulit_id;
      const result = await alergiKulitModel.deleteAlergiKulit(alergi_kulit_id);
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot delete alergi kulit!"
      );
    }
  },
};
