const masalahKulitModel = require("../models/masalah_kulit");
const miscHelper = require("../helpers");
const uniqid = require("uniqid");
const filesystem = require("fs").promises;
const { ip, image_path } = require("../configs");

const { getAllProduct } = require("../helpers/product");

const deleteFile = async (id) => {
  const checkId = await masalahKulitModel.checkId(id);
  const data = checkId[0];
  if (data !== undefined) {
    const path = data.image.replace(`http://${ip}`, `${image_path}`);
    await filesystem.unlink(path);
  }
};

const finalResult = async (result) => {
  const allProduct = await getAllProduct();
  const finalResult = [];
  await result.map((resultValue) => {
    let product = { title: "Product Not Found" };
    allProduct.data.filter((productValue) => {
      if (resultValue.id_product === productValue.id) {
        let newPrice = productValue.price.replace(/[^0-9 ]/g, "");
        return (product = {
          ...productValue,
          price: parseInt(newPrice),
          cart_quantity: 0,
          stock: productValue.stock || 0,
        });
      }
    });
    let tags = [resultValue.tags_image];
    delete resultValue.tags_image;
    delete resultValue.tags_score;
    let newResult = { ...resultValue, tags: tags, product };
    const findIndex = finalResult.findIndex(
      (findIndexValue) => findIndexValue.id === resultValue.id
    );
    if (findIndex < 0) {
      return finalResult.push({
        ...newResult,
        id_product: [product.id],
        product: [product],
      });
    } else {
      return [
        finalResult[findIndex].product.push(product),
        finalResult[findIndex].id_product.push(product.id),
      ];
    }
  });
  return finalResult;
};

module.exports = {
  getMasalahKulit: async (request, response) => {
    try {
      const search_name = request.query.name || "";
      const result = await masalahKulitModel.getMasalahKulit(search_name);
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot get masalah kulit!"
      );
    }
  },
  insertMasalahKulit: async (request, response) => {
    try {
      const id = uniqid.process();
      await request.body.id_product.map(async (value) => {
        let data = {
          id: uniqid.process(),
          id_product: value,
          id_masalah_kulit: id,
        };
        await masalahKulitModel.insertMasalahKulitProduct(data);
      });
      const data = {
        id,
        image: `http://${ip}/upload/masalah_kulit/${request.file.filename}`,
        name: request.body.name,
        description: request.body.description,
        show_masalah: request.body.show_masalah,
        tags_image: request.body.tags_image,
        tags_score: request.body.tags_score,
        date_created: new Date(),
        date_updated: new Date(),
      };
      const result = await masalahKulitModel.insertMasalahKulit(data);
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot insert masalah kulit!"
      );
    }
  },
  updateMasalahKulit: async (request, response) => {
    try {
      const id_masalah_kulit = request.params.id_masalah_kulit;
      await masalahKulitModel.deleteMasalahKulitProduct(id_masalah_kulit);
      await request.body.id_product.map(async (value) => {
        let data = {
          id: uniqid.process(),
          id_product: value,
          id_masalah_kulit: id_masalah_kulit,
        };
        await masalahKulitModel.insertMasalahKulitProduct(data);
      });
      if (!request.file || Object.keys(request.file).length === 0) {
        const data = {
          name: request.body.name,
          description: request.body.description,
          show_masalah: request.body.show_masalah,
          date_updated: new Date(),
        };
        const result = await masalahKulitModel.updateMasalahKulit(
          data,
          id_masalah_kulit
        );
        const final_result = await finalResult(result);
        miscHelper.customResponse(response, 200, final_result);
      } else {
        await deleteFile(id_masalah_kulit);
        const data = {
          image: `http://${ip}/upload/masalah_kulit/${request.file.filename}`,
          name: request.body.name,
          description: request.body.description,
          show_masalah: request.body.show_masalah,
          date_updated: new Date(),
        };
        const result = await masalahKulitModel.updateMasalahKulit(
          data,
          id_masalah_kulit
        );
        const final_result = await finalResult(result);
        miscHelper.customResponse(response, 200, final_result);
      }
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot update masalah kulit!"
      );
    }
  },
  deleteMasalahKulit: async (request, response) => {
    try {
      const id_masalah_kulit = request.params.id_masalah_kulit;
      await deleteFile(id_masalah_kulit);
      await masalahKulitModel.deleteMasalahKulitProduct(id_masalah_kulit);
      const result = await masalahKulitModel.deleteMasalahKulit(
        id_masalah_kulit
      );
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot delete masalah kulit!"
      );
    }
  },
};
