const infoKondisiKulitImageModel = require("../models/info_kondisi_kulit_image");
const miscHelper = require("../helpers");
const filesystem = require("fs").promises;
const { ip, image_path } = require("../configs");
const uniqid = require("uniqid");

const deleteFile = async (id) => {
  const checkId = await infoKondisiKulitImageModel.checkId(id);
  const data = checkId[0];
  if (data !== undefined) {
    const path = data.image.replace(`http://${ip}`, `${image_path}`);
    await filesystem.unlink(path);
  }
};
module.exports = {
  getInfoKondisiKulitImage: async (request, response) => {
    try {
      const result = await infoKondisiKulitImageModel.getInfoKondisiKulitImage();
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot get info kondisi kulit image!"
      );
    }
  },
  insertInfoKondisiKulitImage: async (request, response) => {
    try {
      const data = {
        id: uniqid.process(),
        image: `http://${ip}/upload/info_kondisi_kulit/${request.file.filename}`,
        id_info_kondisi_kulit: request.body.id_info_kondisi_kulit,
        date_created: new Date(),
        date_updated: new Date(),
      };
      const result = await infoKondisiKulitImageModel.insertInfoKondisiKulitImage(
        data
      );
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot insert info kondisi kulit image!"
      );
    }
  },
  updateInfoKondisiKulitImage: async (request, response) => {
    try {
      const info_kondisi_kulit_image_id = request.params.info_kondisi_kulit_image_id;
      await deleteFile(info_kondisi_kulit_image_id);
      const data = {
        image: `http://${ip}/upload/info_kondisi_kulit/${request.file.filename}`,
        date_updated: new Date(),
      };
      const result = await infoKondisiKulitImageModel.updateInfoKondisiKulitImage(
        data,
        info_kondisi_kulit_image_id
      );
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot update info kondisi kulit image!"
      );
    }
  },
  deleteInfoKondisiKulitImage: async (request, response) => {
    try {
      const info_kondisi_kulit_image_id = request.params.info_kondisi_kulit_image_id;
      await deleteFile(info_kondisi_kulit_image_id);
      const result = await infoKondisiKulitImageModel.deleteInfoKondisiKulitImage(
        info_kondisi_kulit_image_id
      );
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot delete info kondisi kulit image!"
      );
    }
  },
};
