const adminModel = require("../models/admin");
const helper = require("../helpers/");
const JWT = require("jsonwebtoken");
const { JWT_KEY, JWT_Refresh } = require("../configs");
const miscHelper = require("../helpers");
const uniqid = require("uniqid");
const tokenList = {};
module.exports = {
  register: async (request, response) => {
    try {
      const salt = helper.generateSalt(18);
      const hashPassword = helper.setPassword(request.body.password, salt);
      const name = request.body.name;
      const id =
        name
          .toLowerCase()
          .replace(/[^a-zA-Z0-9- ]/g, "")
          .split(" ")
          .join("-") +
        "-" +
        uniqid.process();
      const data = {
        id,
        name,
        email: request.body.email,
        salt: hashPassword.salt,
        password: hashPassword.passwordHash,
        date_created: new Date(),
        date_updated: new Date(),
      };
      const result = await adminModel.register(data);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Internal server error!");
    }
  },
  login: async (request, response) => {
    const emailValid = await adminModel.checkEmail(request.body.email);
    const dataAdmin = emailValid[0];
    if (emailValid.length > 0) {
      const data = {
        email: request.body.email,
        password: request.body.password,
      };
      const hashPassword = helper.setPassword(data.password, dataAdmin.salt);
      if (hashPassword.passwordHash === dataAdmin.password) {
        const admin = {
          email: dataAdmin.email,
          id: dataAdmin.id,
        };
        const token = JWT.sign(admin, JWT_KEY, { expiresIn: "1d" });
        const refresh_token = JWT.sign(admin, JWT_Refresh, { expiresIn: "7d" });

        delete dataAdmin.salt;
        delete dataAdmin.password;

        dataAdmin.token = token;
        dataAdmin.refresh_token = refresh_token;

        tokenList[dataAdmin.email] = dataAdmin;
        miscHelper.customResponse(response, 200, dataAdmin);
      } else {
        return miscHelper.customErrorResponse(
          response,
          404,
          "Your password is wrong!"
        );
      }
    } else {
      return miscHelper.customErrorResponse(
        response,
        404,
        "Your email is wrong or not registered!"
      );
    }
  },
  token: async (request, response) => {
    const setData = request.body;
    if (setData.refresh_token === tokenList[setData.email].refresh_token) {
      const admin = {
        email: tokenList.email,
        id: tokenList.id,
      };
      const token = JWT.sign(admin, JWT_KEY, { expiresIn: "1d" });

      tokenList[setData.email].token = token;

      miscHelper.customResponse(response, 200, tokenList[setData.email]);
    } else {
      return miscHelper.customErrorResponse(
        response,
        404,
        "Cannot refresh token!"
      );
    }
  },
  getAllAdmin: async (request, response) => {
    try {
      const result = await adminModel.getAllAdmin(
      );
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Internal server error!");
    }
  },
  updateAdmin: async (request, response) => {
    try {
      const data = {
        name: request.body.name,
        email: request.body.email,
        date_updated: new Date(),
      };
      const id = request.params.id;
      const result = await adminModel.updateAdmin(data, id);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Internal server error!");
    }
  },
  deleteAdmin: async (request, response) => {
    try {
      const id = request.params.id;
      const result = await adminModel.deleteAdmin(id);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Internal server error!");
    }
  },
};
