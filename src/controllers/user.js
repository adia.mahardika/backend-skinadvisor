const userModel = require("../models/user");
const miscHelper = require("../helpers");
const uniqid = require("uniqid");

module.exports = {
  getUser: async (request, response) => {
    try {
      const startDate = request.query.start || "";
      const endDate = request.query.end || "";
      const searchName = request.query.name || "";
      const sortBy = request.query.sort_by || "name";
      const orderBy = request.query.order_by || "ASC";
      const totalData = await userModel.countData(
        searchName,
        sortBy,
        orderBy,
        startDate,
        endDate
      );
      const limit = parseInt(request.query.limit) || 10;
      const page = parseInt(request.query.page) || 1;
      const startIndex = (page - 1) * limit;
      const pagination = {
        totalData,
        page,
        limit,
        startIndex,
      };
      const result = await userModel.getUser(
        startDate,
        endDate,
        searchName,
        sortBy,
        orderBy,
        startIndex,
        limit
        );
        console.log(result)
        miscHelper.customResponsePagination(response, 200, result, pagination);
      } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot get user!");
    }
  },
  insertUser: async (request, response) => {
    try {
      const name = request.body.name;
      const id_user =
        name.toLowerCase().split(" ").join("-") + "-" + uniqid.time();
      const id_masalah_kulit_user = uniqid.process();
      const id_kondisi_kulit_user = uniqid.process();
      const id_alergi_kulit_user = uniqid.process();
      const dataUser = {
        id: id_user,
        name,
        birth_date: request.body.birth_date,
        gender: request.body.gender,
        masalah_kulit_user: id_masalah_kulit_user,
        kondisi_kulit_user: id_kondisi_kulit_user,
        alergi_kulit_user: id_alergi_kulit_user,
        date_created: new Date(),
        date_updated: new Date(),
      };
      request.body.masalah_kulit.map(async (value) => {
        const data = {
          id: id_masalah_kulit_user,
          id_masalah_kulit: value,
          id_user: id_user,
        };
        await userModel.insertMasalahKulitUser(data);
      });
      request.body.kondisi_kulit.map(async (value) => {
        const data = {
          id: id_kondisi_kulit_user,
          id_kondisi_kulit: value,
          id_user: id_user,
        };
        await userModel.insertKondisiKulitUser(data);
      });
      request.body.alergi_kulit.map(async (value) => {
        const data = {
          id: id_alergi_kulit_user,
          id_alergi_kulit: value,
          description: request.body.alergi_kulit_user_description || "",
          id_user: id_user,
        };
        await userModel.insertAlergiKulitUser(data);
      });
      const result = await userModel.insertUser(dataUser);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot insert user!");
    }
  },
  updateUser: async (request, response) => {
    try {
      const user_id = request.params.user_id;
      const data = {
        name: request.body.name,
        birth_date: request.body.birth_date,
        gender: request.body.gender,
        kondisi_kulit: request.body.kondisi_kulit,
        alergi: request.body.alergi,
        masalah: request.body.masalah,
        date_updated: new Date(),
      };
      const result = await userModel.updateUser(data, user_id);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot update user!");
    }
  },
  deleteUser: async (request, response) => {
    try {
      const user_id = request.params.user_id;
      const startDate = request.query.start || "";
      const endDate = request.query.end || "";
      const searchName = request.query.name || "";
      const sortBy = request.query.sort_by || "name";
      const orderBy = request.query.order_by || "ASC";
      const totalData = await userModel.countData(
        searchName,
        sortBy,
        orderBy,
        startDate,
        endDate
      );
      const limit = parseInt(request.query.limit) || 10;
      const page = parseInt(request.query.page) || 1;
      const startIndex = (page - 1) * limit;
      const pagination = {
        totalData,
        page,
        limit,
        startIndex,
      };
      await userModel.deleteAlergiKulitUser(user_id);
      await userModel.deleteMasalahKulitUser(user_id);
      await userModel.deleteKondisiKulitUser(user_id);
      const result = await userModel.deleteUser(user_id);
      miscHelper.customResponsePagination(response, 200, result, pagination);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot delete user!");
    }
  },
};
