const infoKondisiKulitModel = require("../models/info_kondisi_kulit");
const miscHelper = require("../helpers");
const uniqid = require("uniqid");

module.exports = {
  getInfoKondisiKulit: async (request, response) => {
    try {
      const result = await infoKondisiKulitModel.getInfoKondisiKulit();
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot get info kondisi kulit!"
      );
    }
  },
  insertInfoKondisiKulit: async (request, response) => {
    try {
      const title = request.body.title;
      const id = title.toLowerCase().split(" ").join("-") + "-" + uniqid.time();
      const data = {
        id,
        title,
        subtitle: request.body.subtitle,
        date_created: new Date(),
        date_updated: new Date(),
      };
      const result = await infoKondisiKulitModel.insertInfoKondisiKulit(data);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot insert info kondisi kulit!"
      );
    }
  },
  updateInfoKondisiKulit: async (request, response) => {
    try {
      const info_kondisi_kulit_id = request.params.info_kondisi_kulit_id;
      const data = {
        title: request.body.title,
        subtitle: request.body.subtitle,
        date_updated: new Date(),
      };
      const result = await infoKondisiKulitModel.updateInfoKondisiKulit(
        data,
        info_kondisi_kulit_id
      );
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot update info kondisi kulit!"
      );
    }
  },
  deleteInfoKondisiKulit: async (request, response) => {
    try {
      const info_kondisi_kulit_id = request.params.info_kondisi_kulit_id;
      const result = await infoKondisiKulitModel.deleteInfoKondisiKulit(
        info_kondisi_kulit_id
      );
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot delete info kondisi kulit!"
      );
    }
  },
};
