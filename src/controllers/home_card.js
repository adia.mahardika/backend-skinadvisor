const homeCardModel = require("../models/home_card");
const miscHelper = require("../helpers");
const filesystem = require("fs").promises;
const { ip, image_path } = require("../configs");
const uniqid = require("uniqid");

const deleteFile = async (id) => {
  const checkId = await homeCardModel.checkId(id);
  const data = checkId[0];
  if (data !== undefined) {
    const path = data.image.replace(`http://${ip}`, `${image_path}`);
    await filesystem.unlink(path);
  }
};
module.exports = {
  getHomeCard: async (request, response) => {
    try {
      const result = await homeCardModel.getHomeCard();
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot get home card!");
    }
  },
  insertHomeCard: async (request, response) => {
    try {
      const id = uniqid.process();
      const data = {
        id,
        description: request.body.description,
        image: `http://${ip}/upload/home_card/${request.file.filename}`,
        date_created: new Date(),
        date_updated: new Date(),
      };
      const result = await homeCardModel.insertHomeCard(data);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot insert home card!");
    }
  },
  updateHomeCard: async (request, response) => {
    try {
      const home_card_id = request.params.home_card_id;
      if (!request.file || Object.keys(request.file).length === 0) {
        const data = {
          description: request.body.description,
          date_updated: new Date(),
        };
        const result = await homeCardModel.updateHomeCard(data, home_card_id);
        miscHelper.customResponse(response, 200, result);
      } else {
        await deleteFile(home_card_id);
        const data = {
          description: request.body.description,
          image: `http://${ip}/upload/home_card/${request.file.filename}`,
          date_updated: new Date(),
        };
        const result = await homeCardModel.updateHomeCard(data, home_card_id);
        miscHelper.customResponse(response, 200, result);
      }
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot update home card!");
    }
  },
  deleteHomeCard: async (request, response) => {
    try {
      const home_card_id = request.params.home_card_id;
      await deleteFile(home_card_id);
      const result = await homeCardModel.deleteHomeCard(home_card_id);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot delete home card!");
    }
  },
};
