const alergiProductModel = require("../models/alergi_product");
const miscHelper = require("../helpers");
const uniqid = require("uniqid");

const { getAllProduct } = require("../helpers/product");

const finalResult = async (result) => {
  const allProduct = await getAllProduct();
  const finalResult = [];
  await result.map(async (resultValue) => {
    let product = { title: "Product Not Found" };
    await allProduct.data.filter((productValue) => {
      if (resultValue.id_product === productValue.id) {
        return (product = { ...productValue, stock: productValue.stock || 0 });
      }
    });
    let newResult = { ...resultValue, product };
    return finalResult.push({
      ...newResult,
      product: product,
      id_product: product.id,
    });
  });
  return finalResult;
};
module.exports = {
  getAlergiProduct: async (request, response) => {
    try {
      const search_name = request.query.name || "";
      const result = await alergiProductModel.getAlergiProduct(search_name);
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot get alergi product!"
      );
    }
  },
  insertAlergiProduct: async (request, response) => {
    try {
      await request.body.id_product.map(async (value) => {
        const data = {
          id: uniqid.process(),
          id_product: value,
          id_alergi_kulit: request.body.id_alergi_kulit,
        };
        await alergiProductModel.insertAlergiProduct(data);
      });
      const result = await alergiProductModel.getAlergiProduct("");
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot insert alergi product!"
      );
    }
  },
  updateAlergiProduct: async (request, response) => {
    try {
      const alergi_product_id = request.params.alergi_product_id;
      const data = {
        id_product: request.body.id_product,
      };
      const result = await alergiProductModel.updateAlergiProduct(data, alergi_product_id);
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot update alergi product!"
      );
    }
  },
  deleteAlergiProduct: async (request, response) => {
    try {
      const alergi_product_id = request.params.alergi_product_id;
      const result = await alergiProductModel.deleteAlergiProduct(alergi_product_id);
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot delete alergi product!"
      );
    }
  },
};
