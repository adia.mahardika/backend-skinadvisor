const homeModel = require("../models/home");
const miscHelper = require("../helpers");
const filesystem = require("fs").promises;
const { ip, image_path } = require("../configs");
const { compress } = require("../helpers/upload");
const uniqid = require("uniqid");

const deleteFile = async (id) => {
  const checkId = await homeModel.checkId(id);
  const data = checkId[0];
  if (data !== undefined) {
    const path = data.image.replace(`http://${ip}`, `${image_path}`);
    await filesystem.unlink(path);
  }
};
module.exports = {
  getHome: async (request, response) => {
    try {
      const result = await homeModel.getHome();
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error)
      miscHelper.customErrorResponse(response, 404, "Cannot get home!");
    }
  },
  insertHome: async (request, response) => {
    try {
      await compress(request.file.path);
      const title = request.body.title;
      const id =
      title.toLowerCase().split(" ").join("-") + "-" + uniqid.time();
      const data = {
        id,
        title,
        subtitle: request.body.subtitle,
        description: request.body.description,
        image: `http://${ip}/upload/home/${request.file.filename}`,
        date_created: new Date(),
        date_updated: new Date(),
      };
      const result = await homeModel.insertHome(data);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot insert home!");
    }
  },
  updateHome: async (request, response) => {
    try {
      const home_id = request.params.home_id;
      if (!request.file || Object.keys(request.file).length === 0) {
        const data = {
          title: request.body.title,
          subtitle: request.body.subtitle,
          description: request.body.description,
          date_updated: new Date(),
        };
        const result = await homeModel.updateHome(data, home_id);
        miscHelper.customResponse(response, 200, result);
      } else {
        await deleteFile(home_id);
        await compress(request.file.path);
        const data = {
          title: request.body.title,
          subtitle: request.body.subtitle,
          description: request.body.description,
          image: `http://${ip}/upload/home/${request.file.filename}`,
          date_updated: new Date(),
        };
        const result = await homeModel.updateHome(data, home_id);
        miscHelper.customResponse(response, 200, result);
      }
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot update home!");
    }
  },
  deleteHome: async (request, response) => {
    try {
      const home_id = request.params.home_id;
      await deleteFile(home_id);
      const result = await homeModel.deleteHome(home_id);
      miscHelper.customResponse(response, 200, result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(response, 404, "Cannot delete home!");
    }
  },
};
