const kondisiKulitModel = require("../models/kondisi_kulit");
const miscHelper = require("../helpers");
const uniqid = require("uniqid");
const filesystem = require("fs").promises;
const { ip, image_path } = require("../configs");

const { getAllProduct } = require("../helpers/product");

const deleteFile = async (id) => {
  const checkId = await kondisiKulitModel.checkId(id);
  const data = checkId[0];
  if (data !== undefined) {
    const path = data.image.replace(`http://${ip}`, `${image_path}`);
    await filesystem.unlink(path);
  }
};

const finalResult = async (result) => {
  const allProduct = await getAllProduct();
  const finalResult = [];
  await result.map(async (resultValue) => {
    let product = { title: "Product Not Found" };
    await allProduct.data.filter((productValue) => {
      if (resultValue.id_product === productValue.id) {
        let newPrice = productValue.price.replace(/[^0-9 ]/g, "");
        return (product = {
          ...productValue,
          price: parseInt(newPrice),
          cart_quantity: 0,
          stock: productValue.stock || 0,
        });
      }
    });
    let newResult = { ...resultValue, product };
    const findIndex = finalResult.findIndex(
      (findIndexValue) => findIndexValue.id === resultValue.id
    );
    if (findIndex < 0) {
      return finalResult.push({
        ...newResult,
        id_product: [product.id],
        product: [product],
      });
    } else {
      return [
        finalResult[findIndex].product.push(product),
        finalResult[findIndex].id_product.push(product.id),
      ];
    }
  });
  return finalResult;
};

module.exports = {
  getKondisiKulit: async (request, response) => {
    try {
      const search_name = request.query.name || "";
      const result = await kondisiKulitModel.getKondisiKulit(search_name);
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot get kondisi kulit!"
      );
    }
  },
  insertKondisiKulit: async (request, response) => {
    try {
      const id = uniqid.process();
      await request.body.id_product.map(async (value) => {
        let data = {
          id: uniqid.process(),
          id_product: value,
          id_kondisi_kulit: id,
        };
        await kondisiKulitModel.insertKondisiKulitProduct(data);
      });
      const data = {
        id,
        image: `http://${ip}/upload/kondisi_kulit/${request.file.filename}`,
        name: request.body.name,
        description: request.body.description,
        date_created: new Date(),
        date_updated: new Date(),
      };
      const result = await kondisiKulitModel.insertKondisiKulit(data);
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot insert kondisi kulit!"
      );
    }
  },
  updateKondisiKulit: async (request, response) => {
    try {
      const id_kondisi_kulit = request.params.id_kondisi_kulit;
      await kondisiKulitModel.deleteKondisiKulitProduct(id_kondisi_kulit);
      await request.body.id_product.map(async (value) => {
        let data = {
          id: uniqid.process(),
          id_product: value,
          id_kondisi_kulit: id_kondisi_kulit,
        };
        await kondisiKulitModel.insertKondisiKulitProduct(data);
      });
      if (!request.file || Object.keys(request.file).length === 0) {
        const data = {
          name: request.body.name,
          description: request.body.description,
          date_updated: new Date(),
        };
        const result = await kondisiKulitModel.updateKondisiKulit(
          data,
          id_kondisi_kulit
        );
        const final_result = await finalResult(result);
        miscHelper.customResponse(response, 200, final_result);
      } else {
        await deleteFile(id_kondisi_kulit);
        const data = {
          image: `http://${ip}/upload/kondisi_kulit/${request.file.filename}`,
          name: request.body.name,
          description: request.body.description,
          date_updated: new Date(),
        };
        const result = await kondisiKulitModel.updateKondisiKulit(
          data,
          id_kondisi_kulit
        );
        const final_result = await finalResult(result);
        miscHelper.customResponse(response, 200, final_result);
      }
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot update kondisi kulit!"
      );
    }
  },
  deleteKondisiKulit: async (request, response) => {
    try {
      const id_kondisi_kulit = request.params.id_kondisi_kulit;
      await deleteFile(id_kondisi_kulit);
      await kondisiKulitModel.deleteKondisiKulitProduct(id_kondisi_kulit);
      const result = await kondisiKulitModel.deleteKondisiKulit(
        id_kondisi_kulit
      );
      const final_result = await finalResult(result);
      miscHelper.customResponse(response, 200, final_result);
    } catch (error) {
      console.log(error);
      miscHelper.customErrorResponse(
        response,
        404,
        "Cannot delete kondisi kulit!"
      );
    }
  },
};
