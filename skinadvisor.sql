-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2020 at 06:17 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skinadvisor`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `salt` text NOT NULL,
  `password` text NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `salt`, `password`, `date_updated`, `date_created`) VALUES
('admin-huokibv1v5u', 'Admin', 'admin@skinadvisor.com', 'beff1039d5aac9ff29', '6c4aea7c652498120b813923a4ca109984f393fad5f131df8bb5401c4b801a5092ff4d07f53eb790e38395cd423a3dfcf228cb03d0221e003b2747373f9880a7', '2020-12-05 15:37:27', '2020-12-05 15:37:27');

-- --------------------------------------------------------

--
-- Table structure for table `alergi_kulit`
--

CREATE TABLE `alergi_kulit` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `alergi_kulit`
--

INSERT INTO `alergi_kulit` (`id`, `name`, `description`, `date_created`, `date_updated`) VALUES
('564khwwcpbe', 'Parfume', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', '2020-11-25 04:17:19', '2020-11-25 04:17:19'),
('564khwwcyey', 'Alcohol Denat', 'iniiiiiiiiiii', '2020-11-25 04:17:31', '2020-11-29 14:19:01'),
('564khwwd615', 'Tidak Ada', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', '2020-11-25 04:17:41', '2020-11-25 04:17:41'),
('564khwwdh74', 'Lainnya', '-', '2020-11-25 04:17:55', '2020-11-25 04:17:55');

-- --------------------------------------------------------

--
-- Table structure for table `alergi_kulit_user`
--

CREATE TABLE `alergi_kulit_user` (
  `no` int(11) NOT NULL,
  `id` varchar(255) NOT NULL,
  `id_alergi_kulit` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `id_user` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `alergi_kulit_user`
--

INSERT INTO `alergi_kulit_user` (`no`, `id`, `id_alergi_kulit`, `description`, `id_user`) VALUES
(19, '554khwyepxf', '564khwwcyey', 'Alergi Test', 'user-test-khwyepxc'),
(23, '554khx0l5ls', '564khwwcyey', '', 'user-test-khx0l5lp'),
(24, '554khx0lwns', '564khwwcyey', '', 'user-test-khx0lwnp'),
(29, '3j0khxrzedm', '564khwwcyey', '', 'test-2-khxrzedj'),
(36, '74kki0gajy4', '564khwwcyey', '', 'test-dong-ki0gajy1'),
(37, '74kki0gbv6i', '564khwwcyey', '', 'test-dong-ki0gbv6f');

-- --------------------------------------------------------

--
-- Table structure for table `alergi_product`
--

CREATE TABLE `alergi_product` (
  `id` varchar(255) NOT NULL,
  `id_product` int(55) NOT NULL,
  `id_alergi_kulit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `alergi_product`
--

INSERT INTO `alergi_product` (`id`, `id_product`, `id_alergi_kulit`) VALUES
('46oki16yxnu', 2806, '564khwwcpbe'),
('8pski41p6pc', 1266, '564khwwcyey'),
('8pski41p6pn', 4989, '564khwwcyey'),
('f8ki32mvzu', 5010, '564khwwcpbe'),
('hoskibe9yjz', 5010, '564khwwcyey'),
('hoskibebbzv', 4992, '564khwwdh74');

-- --------------------------------------------------------

--
-- Table structure for table `basic_info`
--

CREATE TABLE `basic_info` (
  `id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `basic_info`
--

INSERT INTO `basic_info` (`id`, `title`, `subtitle`, `image`, `date_created`, `date_updated`) VALUES
('skin-advisor-ki5hq7wi', 'Skin Advisor', 'Skin Health is Number One', 'http://localhost:5000/upload/basic_info/16067974877533bea45b41b7d.png', '2020-12-01 04:37:51', '2020-12-01 04:38:07');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`id`, `title`, `subtitle`, `description`, `image`, `date_created`, `date_updated`) VALUES
('skin-advisor-khww3zcq', 'Skin Advisor', 'Skin Health is Number One', 'Hi Sahabat Avo, selamat datang di Skin Advisor by Avoskin. Kamu bisa memanfaatkan fitur kami untuk menganalisa kondisi kulitmu dan mendapatkan rekomendasi produk sesuai kebutuhanmu.', 'http://localhost:5000/upload/home/1606277429400f9f76f54344f.png', '2020-11-25 04:10:32', '2020-11-25 04:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `home_card`
--

CREATE TABLE `home_card` (
  `id` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home_card`
--

INSERT INTO `home_card` (`id`, `image`, `description`, `date_created`, `date_updated`) VALUES
('554khwwmhz4', 'http://localhost:5000/upload/home_card/1606278296690ae1c30bb0363.svg', 'Yuk, kita mulai analisa kulitmu dengan Skin Advisor!', '2020-11-25 04:24:56', '2020-12-02 04:24:05'),
('554khwwow39', 'http://localhost:5000/upload/home_card/160688977792073d5afa21828.svg', 'Yuk, kita mulai analisa kulitmu dengan Skin Advisor! bag2', '2020-11-25 04:26:48', '2020-12-02 06:16:17');

-- --------------------------------------------------------

--
-- Table structure for table `info_kondisi_kulit`
--

CREATE TABLE `info_kondisi_kulit` (
  `id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `info_kondisi_kulit`
--

INSERT INTO `info_kondisi_kulit` (`id`, `title`, `subtitle`, `date_created`, `date_updated`) VALUES
('skin-advisor-ki64e0qa', 'Kondisi Kulit', 'Apakah kamu memiliki masalah pada kulitmu?', '2020-12-01 15:12:13', '2020-12-02 08:38:08');

-- --------------------------------------------------------

--
-- Table structure for table `info_kondisi_kulit_image`
--

CREATE TABLE `info_kondisi_kulit_image` (
  `id` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `id_info_kondisi_kulit` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `info_kondisi_kulit_image`
--

INSERT INTO `info_kondisi_kulit_image` (`id`, `image`, `id_info_kondisi_kulit`, `date_created`, `date_updated`) VALUES
('67gkia2gsis', 'http://localhost:5000/upload/info_kondisi_kulit/160707416841510005ee4ee35.png', 'skin-advisor-ki64e0qa', '2020-12-04 09:29:28', '2020-12-04 09:29:28'),
('67gkia2h1d2', 'http://localhost:5000/upload/info_kondisi_kulit/16070741798711e47b4347104.png', 'skin-advisor-ki64e0qa', '2020-12-04 09:29:39', '2020-12-04 09:29:39'),
('67gkia2h8ix', 'http://localhost:5000/upload/info_kondisi_kulit/1607074189154c564672ea183.png', 'skin-advisor-ki64e0qa', '2020-12-04 09:29:49', '2020-12-04 09:29:49');

-- --------------------------------------------------------

--
-- Table structure for table `kondisi_kulit`
--

CREATE TABLE `kondisi_kulit` (
  `id` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kondisi_kulit`
--

INSERT INTO `kondisi_kulit` (`id`, `image`, `name`, `description`, `date_created`, `date_updated`) VALUES
('564khww95gi', 'http://localhost:5000/upload/kondisi_kulit/1606779572557205db36c6c67.svg', 'Kering', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', '2020-11-25 04:14:33', '2020-11-30 23:39:32'),
('564khwwa8b3', 'http://localhost:5000/upload/kondisi_kulit/16067794353650b76d6b31e70.svg', 'Berminyak', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', '2020-11-25 04:15:24', '2020-12-01 00:25:51'),
('564khwwaofy', 'http://localhost:5000/upload/kondisi_kulit/160677960462617b4d2175872.svg', 'Kombinasi', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', '2020-11-25 04:15:45', '2020-11-30 23:40:04'),
('564khwwb7gp', 'http://localhost:5000/upload/kondisi_kulit/1606779687068d9c7eb7959f4.svg', 'Sensitif', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', '2020-11-25 04:16:09', '2020-11-30 23:41:27'),
('564khwwbmwo', 'http://localhost:5000/upload/kondisi_kulit/16067796541909fff92dfaa73.svg', 'Normal', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', '2020-11-25 04:16:29', '2020-11-30 23:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `kondisi_kulit_product`
--

CREATE TABLE `kondisi_kulit_product` (
  `id` varchar(255) NOT NULL,
  `id_product` int(55) NOT NULL,
  `id_kondisi_kulit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kondisi_kulit_product`
--

INSERT INTO `kondisi_kulit_product` (`id`, `id_product`, `id_kondisi_kulit`) VALUES
('2gwki58q4yz', 2806, '564khwwa8b3'),
('2gwki58q4z0', 5010, '564khwwa8b3'),
('7voki572l94', 5007, '564khww95gi'),
('7voki572l95', 1266, '564khww95gi'),
('7voki573adm', 2806, '564khwwaofy'),
('7voki573adn', 5004, '564khwwaofy'),
('7voki574c18', 5001, '564khwwbmwo'),
('7voki574c19', 4995, '564khwwbmwo'),
('7voki5751ek', 4998, '564khwwb7gp'),
('7voki5751el', 1266, '564khwwb7gp');

-- --------------------------------------------------------

--
-- Table structure for table `kondisi_kulit_user`
--

CREATE TABLE `kondisi_kulit_user` (
  `no` int(11) NOT NULL,
  `id` varchar(255) NOT NULL,
  `id_kondisi_kulit` varchar(255) NOT NULL,
  `id_user` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kondisi_kulit_user`
--

INSERT INTO `kondisi_kulit_user` (`no`, `id`, `id_kondisi_kulit`, `id_user`) VALUES
(21, '554khwyepxe', '564khwwa8b3', 'user-test-khwyepxc'),
(25, '554khx0l5lr', '564khwwa8b3', 'user-test-khx0l5lp'),
(26, '554khx0l5lr', '564khww95gi', 'user-test-khx0l5lp'),
(27, '554khx0lwnr', '564khwwa8b3', 'user-test-khx0lwnp'),
(28, '554khx0lwnr', '564khww95gi', 'user-test-khx0lwnp'),
(29, '554khx0lwnr', '564khwwaofy', 'user-test-khx0lwnp'),
(30, '554khx0lwnr', '564khwwbmwo', 'user-test-khx0lwnp'),
(36, '3j0khxrzedl', '564khwwaofy', 'test-2-khxrzedj'),
(37, '3j0khxrzedl', '564khww95gi', 'test-2-khxrzedj'),
(38, '3j0khxrzedl', '564khwwbmwo', 'test-2-khxrzedj'),
(51, '74kki0gajy3', '564khwwa8b3', 'test-dong-ki0gajy1'),
(52, '74kki0gajy3', '564khww95gi', 'test-dong-ki0gajy1'),
(53, '74kki0gajy3', '564khwwaofy', 'test-dong-ki0gajy1'),
(54, '74kki0gbv6h', '564khwwa8b3', 'test-dong-ki0gbv6f'),
(55, '74kki0gbv6h', '564khww95gi', 'test-dong-ki0gbv6f'),
(56, '74kki0gbv6h', '564khwwaofy', 'test-dong-ki0gbv6f');

-- --------------------------------------------------------

--
-- Table structure for table `masalah_kulit`
--

CREATE TABLE `masalah_kulit` (
  `id` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `show_masalah` varchar(255) NOT NULL,
  `tags_image` varchar(255) NOT NULL,
  `tags_score` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `masalah_kulit`
--

INSERT INTO `masalah_kulit` (`id`, `image`, `name`, `description`, `show_masalah`, `tags_image`, `tags_score`, `date_created`, `date_updated`) VALUES
('564khwweq83', 'http://localhost:5000/upload/masalah_kulit/160672800078327e21c584df2.svg', 'Redness', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', 'Show', 'REDNESS_IMAGE', 'REDNESS_SEVERITY_SCORE_FAST', '2020-11-25 04:18:54', '2020-11-30 09:20:00'),
('564khwwfeih', 'http://localhost:5000/upload/masalah_kulit/160672853661348d7feb41f16.svg', 'Wrinkless', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', 'Show', 'WRINKLES_IMAGE_FAST', 'WRINKLES_SEVERITY_SCORE_FAST', '2020-11-25 04:19:25', '2020-11-30 09:28:56'),
('564khwwfz0r', 'http://localhost:5000/upload/masalah_kulit/1606728495635aed5914ae9bc.svg', 'Spots', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', 'Show', 'SPOTS_IMAGE', 'SPOTS_SEVERITY_SCORE_FAST', '2020-11-25 04:19:52', '2020-11-30 09:28:15'),
('564khwwggoq', 'http://localhost:5000/upload/masalah_kulit/16067284569057fccc08835c5.svg', 'Pores', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', 'Show', 'PORES_IMAGE', 'PORES_SEVERITY_SCORE_FAST', '2020-11-25 04:20:15', '2020-11-30 09:27:36'),
('564khwwh5wl', 'http://localhost:5000/upload/masalah_kulit/1606749847839097bf45f5f87.svg', 'Acne', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', 'Hidden', 'ACNE_IMAGE', 'ACNE_SEVERITY_SCORE_FAST', '2020-11-25 04:20:47', '2020-11-30 15:24:07'),
('bpkki48921b', 'http://localhost:5000/upload/masalah_kulit/16067210881323770e17b4397.svg', 'Uneven', 'disebabkan oleh kelenjar di kulit yang memproduksi sebum berlebih. Sebum berlebih ini dapat menyebabkan kulit berminyak, pori-pori tersumbat, dan jerawat. Minyak sebenarnya penting untuk menjaga kulit tetap tetap lembut dan menjaga kelembaban dan iritasi lingkungan.', 'Show', 'UNEVEN_SKINTONE_IMAGE', 'UNEVEN_SKINTONE_SEVERITY_SCORE_FAST', '2020-11-30 07:24:48', '2020-11-30 07:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `masalah_kulit_product`
--

CREATE TABLE `masalah_kulit_product` (
  `id` varchar(255) NOT NULL,
  `id_product` int(55) NOT NULL,
  `id_masalah_kulit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `masalah_kulit_product`
--

INSERT INTO `masalah_kulit_product` (`id`, `id_product`, `id_masalah_kulit`) VALUES
('5g4ki4pdh9r', 5007, '564khwwh5wl'),
('5g4ki4pdh9s', 4992, '564khwwh5wl'),
('5g4ki4pdh9t', 4989, '564khwwh5wl'),
('7voki4cd7wo', 2806, '564khwweq83'),
('7voki4cd7wp', 5010, '564khwweq83'),
('7voki4cmzxr', 4992, '564khwwggoq'),
('7voki4cmzxs', 5004, '564khwwggoq'),
('7voki4cntrn', 2806, '564khwwfz0r'),
('7voki4cntro', 1266, '564khwwfz0r'),
('7voki4copei', 4995, '564khwwfeih'),
('7voki4copej', 1266, '564khwwfeih'),
('bpkki48926o', 4998, 'bpkki48921b'),
('bpkki48926p', 5001, 'bpkki48921b');

-- --------------------------------------------------------

--
-- Table structure for table `masalah_kulit_user`
--

CREATE TABLE `masalah_kulit_user` (
  `no` int(11) NOT NULL,
  `id` varchar(255) NOT NULL,
  `id_masalah_kulit` varchar(255) NOT NULL,
  `id_user` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `masalah_kulit_user`
--

INSERT INTO `masalah_kulit_user` (`no`, `id`, `id_masalah_kulit`, `id_user`) VALUES
(33, '554khwyepxd', '564khwwh5wl', 'user-test-khwyepxc'),
(34, '554khwyepxd', '564khwwggoq', 'user-test-khwyepxc'),
(41, '554khx0l5lq', '564khwwggoq', 'user-test-khx0l5lp'),
(42, '554khx0l5lq', '564khwweq83', 'user-test-khx0l5lp'),
(43, '554khx0lwnq', '564khwwggoq', 'user-test-khx0lwnp'),
(44, '554khx0lwnq', '564khwweq83', 'user-test-khx0lwnp'),
(49, '3j0khxrzedk', '564khwweq83', 'test-2-khxrzedj'),
(50, '3j0khxrzedk', '564khwwfz0r', 'test-2-khxrzedj'),
(63, '74kki0gajy2', '564khwwh5wl', 'test-dong-ki0gajy1'),
(64, '74kki0gbv6g', '564khwwh5wl', 'test-dong-ki0gbv6f');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birth_date` text NOT NULL,
  `gender` varchar(255) NOT NULL,
  `kondisi_kulit_user` varchar(255) NOT NULL,
  `alergi_kulit_user` varchar(255) NOT NULL,
  `masalah_kulit_user` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `birth_date`, `gender`, `kondisi_kulit_user`, `alergi_kulit_user`, `masalah_kulit_user`, `date_created`, `date_updated`) VALUES
('test-2-khxrzedj', 'Test 2', '04-11-2020', 'Woman', '3j0khxrzedl', '3j0khxrzedm', '3j0khxrzedk', '2020-11-25 19:02:46', '2020-11-25 19:02:46'),
('test-dong-ki0gajy1', 'Test Dong', '18-11-2020', 'Man', '74kki0gajy3', '74kki0gajy4', '74kki0gajy2', '2020-11-27 15:58:50', '2020-11-27 15:58:50'),
('test-dong-ki0gbv6f', 'Test Dong', '18-11-2020', 'Man', '74kki0gbv6h', '74kki0gbv6i', '74kki0gbv6g', '2020-11-27 15:59:51', '2020-11-27 15:59:51'),
('user-test-khwyepxc', 'User Test', '03-11-2020', 'Man', '554khwyepxe', '554khwyepxf', '554khwyepxd', '2020-11-25 05:14:52', '2020-11-25 05:14:52'),
('user-test-khx0l5lp', 'User Test', '07-11-2020', 'Woman', '554khx0l5lr', '554khx0l5ls', '554khx0l5lq', '2020-11-25 06:15:52', '2020-11-25 06:15:52'),
('user-test-khx0lwnp', 'User Test', '07-11-2020', 'Woman', '554khx0lwnr', '554khx0lwns', '554khx0lwnq', '2020-11-25 06:16:27', '2020-11-25 06:16:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alergi_kulit`
--
ALTER TABLE `alergi_kulit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alergi_kulit_user`
--
ALTER TABLE `alergi_kulit_user`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `alergi_product`
--
ALTER TABLE `alergi_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `basic_info`
--
ALTER TABLE `basic_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_card`
--
ALTER TABLE `home_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `info_kondisi_kulit_image`
--
ALTER TABLE `info_kondisi_kulit_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kondisi_kulit`
--
ALTER TABLE `kondisi_kulit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kondisi_kulit_product`
--
ALTER TABLE `kondisi_kulit_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kondisi_kulit_user`
--
ALTER TABLE `kondisi_kulit_user`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `masalah_kulit`
--
ALTER TABLE `masalah_kulit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masalah_kulit_product`
--
ALTER TABLE `masalah_kulit_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masalah_kulit_user`
--
ALTER TABLE `masalah_kulit_user`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alergi_kulit_user`
--
ALTER TABLE `alergi_kulit_user`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `kondisi_kulit_user`
--
ALTER TABLE `kondisi_kulit_user`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `masalah_kulit_user`
--
ALTER TABLE `masalah_kulit_user`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
